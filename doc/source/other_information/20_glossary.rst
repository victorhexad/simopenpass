..
  *******************************************************************************
  Copyright (c) 2021 in-tech GmbH

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

Glossary
========

.. glossary::

   ADAS
     Advanced Driving Assistance Systems

   AEB
     Autonomous Emergency Brake
     preventive emergency braking in danger (support or independent launch)

   Channel
     Connects components within an agent (using unique ID)   

   Component
     A module that is part of an agent's equipment   

   COG
     Center of gravity

   Dynamics
     Calculation of dynamic parameters, e.g. position of Agent

   FMI
     Functional Mock-up Interface, see https://fmi-standard.org

   FMU
     Functional Mock-up Unit, see https://fmi-standard.org

   GIDAS
     German In-Depth Accident Study for in-depth traffic accident data collection, see https://www.gidas.org

   MinGW
     Complete Open Source programming tool set, suitable for the development of native MS-Windows applications.

   Model
     An abstract representation of a real object which might omit details e.g. ADAS, driving dynamics, pedestrian, environmental conditions. In the PreCASE framework, a model consists of one or more modules as well as channels connecting them.

   Module
     A dynamic library that is loaded and whose interface functions are called by the framework. Modules contain models or other individual functionality necessary for the simulation. Modules are exchangeable and can be fitted to various purposes ensuring ahigh flexibility of the framework.   

   MSYS
     Collection of GNU utilities (e.g. bash, make, gcc) to allow building of programs which depend on traditionally UNIX tools to be present. It is intended to supplement :term:`MinGW`.

   MSYS2
     Independent rewrite of :term:`MSYS`, based on Cygwin (POSIX compatibility layer) and MinGW-w64.

   OD
     openDRIVE

   openPASS
     Open Platform for Assessment of Safety Systems

   OSI
     A generic interface for the environmental perception of automated driving functions in virtual scenarios.
     See https://opensimulationinterface.github.io/osi-documentation

   PCM
     Pre-Crash Matrix is a specified format which can be used to describe the phase of a road traffic accident before the first collision happens (the so-called pre-crash phase).
     See https://www.vufo.de/pcm/?lang=en

   PreCASE
     Framework for the Simulative Evaluation of Active Safety Systems in Vehicles (OpenPASS predecessor).  

   Scenario
     A set of similar traffic situations.

   TTC
     Time to collision

