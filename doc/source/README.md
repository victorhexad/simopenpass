<!---************************************************************
Copyright (c) 2021 in-tech GmbH
Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
************************************************************-->

# openPASS Documentation

Build this documentation with Sphinx, a python based documentation generator based on [reStructuredText](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html) as primary import format.

Sphinx
======

## Resources

- [reStructuredText Primer](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html)
- [A "How to" Guide for Sphinx + ReadTheDocs](https://source-rtd-tutorial.readthedocs.io/en/latest/index.html)
- [Sphinx Documentation](https://www.sphinx-doc.org)
- [OSI Sphinx Config](https://github.com/OpenSimulationInterface/osi-documentation/blob/master/conf.py)


## Building the documentation (Windows)

1. Get MSYS2 from https://www.msys2.org/
2. Start MSYS2 MSYS and update the system packages:

```
pacman -Syuu
```

If the upgrade requires a restart of MSYS2, resume the upgrade by re-opening the shell and call:

```
pacman -Suu
```

3. Install required packages: Start MSYS2 MinGW 64bit and execute

```
pacman -S mingw-w64-x86_64-cmake        #Tested with 3.19.2-1
pacman -S make                          #Tested with 4.3-1
pacman -S mingw-w64-x86_64-gcc          #Tested with 10.2.0-6
pacman -S mingw-w64-x86_64-python-sphinx
pacman -S mingw-w64-x86_64-python-pip   #Tested with 21.1.3-2
pacman -S mingw-w64-x86_64-python-lxml  #Tested with 4.6.2-2
wget -P /mingw64/share/texmf-dist/tex/latex/anyfontsize http://mirrors.ctan.org/macros/latex/contrib/anyfontsize/anyfontsize.sty
pacman -S mingw-w64-x86_64-zziplib      #Tested with 0.13.72-3
pacman -S mingw-w64-x86_64-texlive-bin  #Tested with 2021.20210424-5
pacman -S mingw-w64-x86_64-texlive-core #Tested with 2021.20210519-2
pacman -S mingw-w64-x86_64-texlive-font-utils #Tested with 2021.20210519-1
pip3 install sphinx-rtd-theme sphinx-tabs
```

4. Create a directory named `build` inside your checked out repository and navigate to it in the MSYS2 MinGW 64bit shell
5. Execute

```
cmake -G "MSYS Makefiles" -DWITH_DOC=ON -DWITH_SIMCORE=OFF -DWITH_TESTS=OFF ..
make doc
```

## Building the documentation (Debian)

```
# install python, pip, spellchecker, ...
sudo apt install doxygen python3 python3-pip dvipng

# install sphinx and its extensions
pip3 install sphinx sphinx-rtd-theme sphinx-tabs

# build doc (only)
mkdir build
cd build
cmake -DWITH_SIMCORE=OFF -DWITH_TESTS=OFF -DWITH_DOC=ON ..
make doc
```
